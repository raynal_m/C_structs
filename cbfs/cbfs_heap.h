#ifndef CBFS_HEAP_DOT_H
#define CBFS_HEAP_DOT_H

#include <stdlib.h>
#include <stdbool.h>

#define HEAP_INIT_SIZE 16

typedef int heap_data_t;

typedef bool heap_cmp_func(heap_data_t, heap_data_t);

typedef struct{
    heap_data_t *nodes;
    heap_cmp_func *compar;
    int alloc_size, nb_elem;
} heap_t;


typedef struct{
    int nb_heaps;
    heap_t **heaps;
    int pop_index;
    int nb_elem;
} cbfs_heap_t;

heap_t *heap_init(heap_cmp_func *compar);

void heap_free(heap_t *heap);

void heap_push(heap_t *heap, heap_data_t data);

heap_data_t heap_pop(heap_t *heap);

cbfs_heap_t *cbfs_heap_init(int nb_heaps, heap_cmp_func **compar_funcs);

void cbfs_heap_free(cbfs_heap_t *cbfs_heap);

void cbfs_heap_push(cbfs_heap_t *cbfs_heap, heap_data_t data, int index);

heap_data_t cbfs_heap_pop(cbfs_heap_t *cbfs_heap);

#endif
