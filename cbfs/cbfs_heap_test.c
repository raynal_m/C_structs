#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "cbfs_heap.h"

#define DEFAULT_NB_HEAPS 577
#define DEFAULT_TEST_SIZE 743512


bool cbfs_heap_test_cmp_func(int n1, int n2){
    return (n1 < n2);
}

int main (int argc, char * argv[]){
    int nb_heaps, test_size;
    if (argc > 2){
        nb_heaps = atoi(argv[1]);
        test_size = atoi(argv[2]);
    } else {
        nb_heaps = DEFAULT_NB_HEAPS;
        test_size = DEFAULT_TEST_SIZE;
    }

    heap_cmp_func ** compar_funcs = malloc(nb_heaps * sizeof(heap_cmp_func*));
    for (int i = 0; i < nb_heaps; i++){
        compar_funcs[i] = cbfs_heap_test_cmp_func;
    }

    cbfs_heap_t *cbfs_heap = cbfs_heap_init(nb_heaps, compar_funcs);
    assert(cbfs_heap->nb_heaps == nb_heaps);
    assert(cbfs_heap->nb_elem == 0);

    int *T = malloc(test_size * sizeof(int));
    for (int i = 0; i < test_size; i++){
        T[i] = i;
    }

    for (int i = 0; i < test_size; i++){
        assert(cbfs_heap->nb_elem == i);
        cbfs_heap_push(cbfs_heap, T[i], (i % nb_heaps));
    }

    int popped;
    int j = 0;
    while ((popped = cbfs_heap_pop(cbfs_heap)) != NULL){
        assert(popped == j);
        j ++;
        assert(cbfs_heap->nb_elem == --test_size);
    }
    return 0;
}
