#include <stdlib.h>

#include "heap.h"


heap_t *heap_init(heap_cmp_func *compar){
    heap_t *heap = malloc(sizeof(heap_t));
    heap->nodes = malloc(HEAP_INIT_SIZE * sizeof(heap_data_t));
    heap->alloc_size = HEAP_INIT_SIZE;
    heap->compar = compar;
    heap->nb_elem = 0;
    return heap;
}


void heap_free(heap_t *heap){
    free(heap->nodes);
    free(heap);
}


void heap_push(heap_t *heap,  heap_data_t data){
    heap->nb_elem ++;
    if (heap->nb_elem == heap->alloc_size){
        heap->nodes = realloc((void *)(heap->nodes),
                              2*heap->alloc_size*sizeof(heap_data_t *));
        heap->alloc_size *= 2;
    }
    int i = heap->nb_elem;
    while (i > 1){
        if (!heap->compar(data, heap->nodes[i/2])){
            break;
        }
        heap->nodes[i] = heap->nodes[i/2];
        i /= 2;
    }
    heap->nodes[i] = data;
}


heap_data_t heap_pop(heap_t *heap){
    if (heap->nb_elem == 0){
        return NULL;
    }
    heap_data_t data = heap->nodes[1];
    int i = 1, j;
    while (1){
        if (2*i < heap->nb_elem){
            j = (heap->compar(heap->nodes[2*i], heap->nodes[2*i+1]) ?
                 2*i :
                 2*i+1);
            if (heap->compar(heap->nodes[heap->nb_elem], heap->nodes[j])){
                break;
            }
        } else if (2*i > heap->nb_elem) {
            break;
        } else if (!heap->compar(heap->nodes[heap->nb_elem], heap->nodes[2*i])){
            j = 2*i;
        } else {
            break;
        }
        heap->nodes[i] = heap->nodes[j];
        i = j;
    }
    heap->nodes[i] = heap->nodes[heap->nb_elem];
    heap->nb_elem --;
    return data;
}
